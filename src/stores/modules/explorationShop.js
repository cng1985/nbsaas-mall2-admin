import {defineStore} from 'pinia'

export const searchStore = defineStore('explorationShop', {

    state: () => {
        return {
            searchObject: {
                no: 1,
                size: 10,
                name: '',
                shopCatalog: '',
                address: '',
                shopState: '',
                shopType:"exploration"
            }
        }
    },
    getters: {
        searchData: (state) => state.searchObject,
    },
    actions: {
        updateSearchObject(obj) {
            this.searchObject = obj;
        }
    },
})
