import {useRoute, useRouter} from "vue-router";
import {onMounted, ref} from "vue";

export function useLayoutView() {
    const selectIndex=ref("");
    const activeIndex=ref("1");


    const route = useRoute()
    const router = useRouter();

    onMounted(async () => {
        if (route.query.activeIndex){
            activeIndex.value = route.query.activeIndex;
        }
        selectIndex.value = route.query.id;
    })
    const  goBack=()=> {
        router.go(-1);
    }
    return {selectIndex,activeIndex,goBack}

}
