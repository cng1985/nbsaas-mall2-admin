import http from "@/utils/request";
import {ElMessage, ElMessageBox} from "element-plus";


export function useDeleteData(functionMethod,callBack) {


    const deleteData=async (row) => {

        try {
            await ElMessageBox.confirm(
                '是否删除该条数据?',
                'Warning',
                {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning',
                }
            )
            let params = {};
            params.id = row.id;
            let res = await http.post(`/${functionMethod}/delete`, params);
            if (res.code === 200) {
                ElMessage({
                    message: '删除成功',
                    type: 'success',
                })
               if (callBack){
                   callBack();
               }
            } else {
                ElMessage.error(res.msg)
            }
        }catch (e){}



    }
    return {deleteData}

}
