import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function fetchOne(functionMethod, searchObject) {
	//集合
	let data = ref({})

    const loadData=async ()=>{
		let param={}
		param.model=functionMethod;
		param.filters=searchObject
		let res = await http.post("/data/selectOne", param);
		if (res.code === 200) {
			data.value=res.data;
		}

	}
	onMounted(async () => {
		await loadData()
	})
	return {
		data,loadData
	}
}

export function fetchList(functionMethod, searchObject) {
	//集合
	let data = ref({})

    const loadData=async ()=>{
		let param={}
		param.model=functionMethod;
		param.filters=searchObject
		let res = await http.post("/data/list", param);
		if (res.code === 200) {
			data.value=res.data;
		}

	}
	onMounted(async () => {

		await loadData()
	})
	return {
		data,loadData
	}
}
