import {onMounted, ref} from "vue";
import http from "@/utils/request";

export function useData(functionMethod, param) {

    let listData = ref({})

    if (param == null) {
        param = {};
        param.sortMethod = "asc";
        param.sortField = "id";
        param.level = 1;
        param.size = 500;
        param.fetch = 0;
    }


    onMounted(() => {
        http.post(functionMethod, param).then(res => {
            if (res.code === 200) {
                listData.value = res.data;
            }
        })
    });

    return {listData}

}