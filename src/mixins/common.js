export default {
    methods: {
        goBack() {
            this.$router.go(-1);
        },
        async getShopList() {
            let self = this;
            let param = {};
            param.sortMethod = "asc";
            param.sortField = "id";
            param.level = 1;
            param.size = 500;
            param.fetch = 0;
            let url = self.$CONFIG.REMOTE_URL + "/rest/shop/list.htm";
            let res = await this.$http.form(url, param);
            if (res.code == 0) {
                self.shopOptions = res.list;
            }
        },
        postData(url, param, callback) {
            let self=this;
            this.$http.form(url, param).then((res) => {
                if (res.code == 0) {
                    callback(res);
                } else {
                    self.$message.error(res.msg);
                }
            });
        },
        postJsonData(url, param, callback) {
            let self=this;
            this.$http.post(url, param).then((res) => {
                if (res.code == 0) {
                    callback(res);
                } else {
                    self.$message.error(res.msg);
                }
            });
        },
        sizeChange(event) {
            this.searchObject.size = event;
            this.getSearchList();
        },
        pageChange(index) {
            this.searchObject.no = index;
            this.getSearchList();
        },
        search() {
            this.searchObject.no = 1;
            this.getSearchList();
        },
        changeTableSort (column) {
            this.searchObject.sortField = column.prop;
            if ("descending" == column.order) {
                this.searchObject.sortMethod = "desc";
            } else if ("ascending" == column.order) {
                this.searchObject.sortMethod = "asc";
            } else {
                this.searchObject.sortMethod = "";
            }
            this.getSearchList();
        }
    },
}
