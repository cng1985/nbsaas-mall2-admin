export default [
    {
        name: "taskfolderitem_home",
        path: "/taskfolderitem/index",
        component: () => import("@/views/pages/taskfolderitem/index.vue"),
        meta: {
            title: "任务项管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolderitem_add",
        path: "/taskfolderitem/add",
        component: () => import("@/views/pages/taskfolderitem/add.vue"),
        meta: {
            title: "添加任务项",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolderitem_update",
        path: "/taskfolderitem/update",
        component: () => import("@/views/pages/taskfolderitem/update.vue"),
        meta: {
            title: "更新任务项",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "taskfolderitem_view",
        path: "/taskfolderitem/view",
        component: () => import("@/views/pages/taskfolderitem/view.vue"),
        meta: {
            title: "任务项详情",
            icon: "el-icon-platform-eleme"
        }
    },
]