export default [
    {
        name: "billboard_home",
        path: "/billboard/index",
        component: () => import("@/views/pages/billboard/index.vue"),
        meta: {
            title: "积分榜管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboard_add",
        path: "/billboard/add",
        component: () => import("@/views/pages/billboard/add.vue"),
        meta: {
            title: "添加积分榜",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboard_update",
        path: "/billboard/update",
        component: () => import("@/views/pages/billboard/update.vue"),
        meta: {
            title: "更新积分榜",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboard_layout",
        path: "/billboard/view_layout",
        component: () => import("@/views/pages/billboard/view_layout.vue"),
        meta: {
            title: "积分榜详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "billboard_view",
                path: "/billboard/view",
                component: () => import("@/views/pages/billboard/view.vue"),
                meta: {
                    title: "积分榜详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]