import tool from './utils/tool'
import http from "./utils/request"
import config from "./config"
import errorHandler from './utils/errorHandler'

export default {
    install(app) {
        app.config.globalProperties.$tool = tool;
        app.config.globalProperties.$http = http;
        app.config.globalProperties.$CONFIG = config;
        app.config.errorHandler = errorHandler

    }
}
