export default [
    {
        name: "organization_home",
        path: "/organization/index",
        component: () => import("@/views/pages/organization/index.vue"),
        meta: {
            title: "组织架构管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "organization_add",
        path: "/organization/add",
        component: () => import("@/views/pages/organization/add.vue"),
        meta: {
            title: "添加组织架构",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "organization_update",
        path: "/organization/update",
        component: () => import("@/views/pages/organization/update.vue"),
        meta: {
            title: "更新组织架构",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "organization_view",
        path: "/organization/view",
        component: () => import("@/views/pages/organization/view.vue"),
        meta: {
            title: "组织架构详情",
            icon: "el-icon-platform-eleme"
        }
    },
]