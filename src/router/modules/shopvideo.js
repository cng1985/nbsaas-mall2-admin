export default [
    {
        name: "shopvideo_home",
        path: "/shopvideo/index",
        component: () => import("@/views/pages/shopvideo/index.vue"),
        meta: {
            title: "商家视频管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopvideo_add",
        path: "/shopvideo/add",
        component: () => import("@/views/pages/shopvideo/add.vue"),
        meta: {
            title: "添加商家视频",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopvideo_update",
        path: "/shopvideo/update",
        component: () => import("@/views/pages/shopvideo/update.vue"),
        meta: {
            title: "更新商家视频",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopvideo_view",
        path: "/shopvideo/view",
        component: () => import("@/views/pages/shopvideo/view.vue"),
        meta: {
            title: "商家视频详情",
            icon: "el-icon-platform-eleme"
        }
    },
]