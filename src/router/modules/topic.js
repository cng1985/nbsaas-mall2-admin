export default [
    {
        name: "topic_home",
        path: "/topic/index",
        component: () => import("@/views/pages/topic/index.vue"),
        meta: {
            title: "通告管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topic_add",
        path: "/topic/add",
        component: () => import("@/views/pages/topic/add.vue"),
        meta: {
            title: "添加通告",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topic_update",
        path: "/topic/update",
        component: () => import("@/views/pages/topic/update.vue"),
        meta: {
            title: "更新通告",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topic_layout",
        path: "/topic/view_layout",
        component: () => import("@/views/pages/topic/view_layout.vue"),
        meta: {
            title: "通告详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "topic_view",
                path: "/topic/view",
                component: () => import("@/views/pages/topic/view.vue"),
                meta: {
                    title: "通告详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "topic_join",
                path: "/topic/join",
                component: () => import("@/views/pages/topic/view_join.vue"),
                meta: {
                    title: "通告详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "view_join_shop",
                path: "/topic/join_shop",
                component: () => import("@/views/pages/topic/view_join_shop.vue"),
                meta: {
                    title: "打卡详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]
