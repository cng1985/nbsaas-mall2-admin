export default [
    {
        name: "area_home",
        path: "/area/index",
        component: () => import("@/views/pages/area/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]
