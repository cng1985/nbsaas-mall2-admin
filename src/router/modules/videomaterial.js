export default [
    {
        name: "videomaterial_home",
        path: "/videomaterial/index",
        component: () => import("@/views/pages/videomaterial/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "videomaterial_add",
        path: "/videomaterial/add",
        component: () => import("@/views/pages/videomaterial/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "videomaterial_update",
        path: "/videomaterial/update",
        component: () => import("@/views/pages/videomaterial/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "videomaterial_view",
        path: "/videomaterial/view",
        component: () => import("@/views/pages/videomaterial/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]