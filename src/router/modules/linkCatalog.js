export default [
    {
        name: "linkCatalog_home",
        path: "/linkCatalog/index",
        component: () => import("@/views/pages/linkCatalog/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]
