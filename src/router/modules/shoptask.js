export default [
    {
        name: "shoptask_home",
        path: "/shoptask/index",
        component: () => import("@/views/pages/shoptask/index.vue"),
        meta: {
            title: "商家任务管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shoptask_add",
        path: "/shoptask/add",
        component: () => import("@/views/pages/shoptask/add.vue"),
        meta: {
            title: "添加商家任务",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shoptask_update",
        path: "/shoptask/update",
        component: () => import("@/views/pages/shoptask/update.vue"),
        meta: {
            title: "更新商家任务",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shoptask_view",
        path: "/shoptask/view",
        component: () => import("@/views/pages/shoptask/view.vue"),
        meta: {
            title: "商家任务详情",
            icon: "el-icon-platform-eleme"
        }
    },
]