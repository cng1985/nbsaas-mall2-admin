export default [
    {
        name: "ad_home",
        path: "/ad/index",
        component: () => import("@/views/pages/ad/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_add",
        path: "/ad/add",
        component: () => import("@/views/pages/ad/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_update",
        path: "/ad/update",
        component: () => import("@/views/pages/ad/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "ad_view",
        path: "/ad/view",
        component: () => import("@/views/pages/ad/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    }
]