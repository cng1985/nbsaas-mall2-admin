export default [
    {
        name: "employee_home",
        path: "/employee/index",
        component: () => import("@/views/pages/employee/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_add",
        path: "/employee/add",
        component: () => import("@/views/pages/employee/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_update",
        path: "/employee/update",
        component: () => import("@/views/pages/employee/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "employee_layout",
        path: "/employee/view_layout",
        component: () => import("@/views/pages/employee/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "employee_view",
                path: "/employee/view",
                component: () => import("@/views/pages/employee/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "employee_restPassword",
                path: "/employee/restPassword",
                component: () => import("@/views/pages/employee/view_restPassword.vue"),
                meta: {
                    title: "重置密码",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]
