export default [
    {
        name: "occupation_home",
        path: "/occupation/index",
        component: () => import("@/views/pages/occupation/index.vue"),
        meta: {
            title: "职业管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "occupation_add",
        path: "/occupation/add",
        component: () => import("@/views/pages/occupation/add.vue"),
        meta: {
            title: "添加职业",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "occupation_update",
        path: "/occupation/update",
        component: () => import("@/views/pages/occupation/update.vue"),
        meta: {
            title: "更新职业",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "occupation_view",
        path: "/occupation/view",
        component: () => import("@/views/pages/occupation/view.vue"),
        meta: {
            title: "职业详情",
            icon: "el-icon-platform-eleme"
        }
    },
]