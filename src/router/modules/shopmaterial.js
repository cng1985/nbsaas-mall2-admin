export default [
    {
        name: "shopmaterial_home",
        path: "/shopmaterial/index",
        component: () => import("@/views/pages/shopmaterial/index.vue"),
        meta: {
            title: "商家素材管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterial_add",
        path: "/shopmaterial/add",
        component: () => import("@/views/pages/shopmaterial/add.vue"),
        meta: {
            title: "添加商家素材",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterial_update",
        path: "/shopmaterial/update",
        component: () => import("@/views/pages/shopmaterial/update.vue"),
        meta: {
            title: "更新商家素材",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopmaterial_view",
        path: "/shopmaterial/view",
        component: () => import("@/views/pages/shopmaterial/view.vue"),
        meta: {
            title: "商家素材详情",
            icon: "el-icon-platform-eleme"
        }
    },
]