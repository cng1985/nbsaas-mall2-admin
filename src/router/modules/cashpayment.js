export default [
    {
        name: "cashpayment_home",
        path: "/cashpayment/index",
        component: () => import("@/views/pages/cashpayment/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashpayment_add",
        path: "/cashpayment/add",
        component: () => import("@/views/pages/cashpayment/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashpayment_update",
        path: "/cashpayment/update",
        component: () => import("@/views/pages/cashpayment/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "cashpayment_view",
        path: "/cashpayment/view",
        component: () => import("@/views/pages/cashpayment/view.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        }
    },
]