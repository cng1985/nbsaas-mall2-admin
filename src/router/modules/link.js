export default [
{
name: "link_home",
path: "/link/index",
component: () => import("@/views/pages/link/index.vue"),
meta: {
title: "管理",
icon: "el-icon-platform-eleme"
}
},
{
name: "link_add",
path: "/link/add",
component: () => import("@/views/pages/link/add.vue"),
meta: {
title: "添加",
icon: "el-icon-platform-eleme"
}
},
{
name: "link_update",
path: "/link/update",
component: () => import("@/views/pages/link/update.vue"),
meta: {
title: "更新",
icon: "el-icon-platform-eleme"
}
},
{
name: "link_view",
path: "/link/view",
component: () => import("@/views/pages/link/view.vue"),
meta: {
title: "详情",
icon: "el-icon-platform-eleme"
}
}
]