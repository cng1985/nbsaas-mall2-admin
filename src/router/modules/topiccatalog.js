export default [
    {
        name: "topiccatalog_home",
        path: "/topiccatalog/index",
        component: () => import("@/views/pages/topiccatalog/index.vue"),
        meta: {
            title: "通告分类管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "topiccatalog_view",
        path: "/topiccatalog/view",
        component: () => import("@/views/pages/topiccatalog/view.vue"),
        meta: {
            title: "通告分类详情",
            icon: "el-icon-platform-eleme"
        }
    },
]