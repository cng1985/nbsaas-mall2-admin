export default [
    {
        name: "shopChain_home",
        path: "/shopChain/index",
        component: () => import("@/views/pages/shopChain/index.vue"),
        meta: {
            title: "商家管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopChain_add",
        path: "/shopChain/add",
        component: () => import("@/views/pages/shopChain/add.vue"),
        meta: {
            title: "添加商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopChain_update",
        path: "/shopChain/update",
        component: () => import("@/views/pages/shopChain/update.vue"),
        meta: {
            title: "更新商家",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "shopChain_layout",
        path: "/shopChain/view_layout",
        component: () => import("@/views/pages/shopChain/view_layout.vue"),
        meta: {
            title: "商家详情",
            icon: "el-icon-platform-eleme"
        },
        children:[
            {
                name: "shopChain_view",
                path: "/shopChain/view",
                component: () => import("@/views/pages/shopChain/view.vue"),
                meta: {
                    title: "商家详情",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]

    }
]
