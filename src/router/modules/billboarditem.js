export default [
    {
        name: "billboarditem_home",
        path: "/billboarditem/index",
        component: () => import("@/views/pages/billboarditem/index.vue"),
        meta: {
            title: "签名项管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboarditem_add",
        path: "/billboarditem/add",
        component: () => import("@/views/pages/billboarditem/add.vue"),
        meta: {
            title: "添加签名项",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboarditem_update",
        path: "/billboarditem/update",
        component: () => import("@/views/pages/billboarditem/update.vue"),
        meta: {
            title: "更新签名项",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "billboarditem_view",
        path: "/billboarditem/view",
        component: () => import("@/views/pages/billboarditem/view.vue"),
        meta: {
            title: "签名项详情",
            icon: "el-icon-platform-eleme"
        }
    },
]