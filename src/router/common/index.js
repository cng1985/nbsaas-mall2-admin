export default [
    {
        name: "usercenter_home_",
        path: "/usercenter/index",
        component: () => import("@/views/common/user/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    }
]