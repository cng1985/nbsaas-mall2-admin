export default [
    {
        name: "tenantuserrole_home",
        path: "/tenantuserrole/index",
        component: () => import("@/views/common/tenantuserrole/index.vue"),
        meta: {
            title: "角色管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_add",
        path: "/tenantuserrole/add",
        component: () => import("@/views/common/tenantuserrole/add.vue"),
        meta: {
            title: "添加角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_update",
        path: "/tenantuserrole/update",
        component: () => import("@/views/common/tenantuserrole/update.vue"),
        meta: {
            title: "更新角色",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "tenantuserrole_view",
        path: "/tenantuserrole/view",
        component: () => import("@/views/common/tenantuserrole/view.vue"),
        meta: {
            title: "角色详情",
            icon: "el-icon-platform-eleme"
        }
    },
]