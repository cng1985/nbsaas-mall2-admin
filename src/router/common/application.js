export default [
    {
        name: "application_home",
        path: "/application/index",
        component: () => import("@/views/pages/application/index.vue"),
        meta: {
            title: "管理",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "application_add",
        path: "/application/add",
        component: () => import("@/views/pages/application/add.vue"),
        meta: {
            title: "添加",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "application_update",
        path: "/application/update",
        component: () => import("@/views/pages/application/update.vue"),
        meta: {
            title: "更新",
            icon: "el-icon-platform-eleme"
        }
    },
    {
        name: "application_layout",
        path: "/application/view_layout",
        component: () => import("@/views/pages/application/view_layout.vue"),
        meta: {
            title: "详情",
            icon: "el-icon-platform-eleme"
        },
        children: [
            {
                name: "application_view",
                path: "/application/view",
                component: () => import("@/views/pages/application/view.vue"),
                meta: {
                    title: "详情",
                    icon: "el-icon-platform-eleme"
                }
            },
            {
                name: "application_menus",
                path: "/application/view_menus",
                component: () => import("@/views/pages/application/view_menus.vue"),
                meta: {
                    title: "应用菜单管理",
                    icon: "el-icon-platform-eleme"
                }
            }
        ]
    }
]
